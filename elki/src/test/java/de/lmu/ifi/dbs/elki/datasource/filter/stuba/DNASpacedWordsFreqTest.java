package de.lmu.ifi.dbs.elki.datasource.filter.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Tomas Farkas
 * 
 */
public class DNASpacedWordsFreqTest {

  char[] data;
  
  @Before
  public void prepareData(){
//    data = new char[]{0,1,1,3,2,2,1,0,3,1,1,0,0,2,3,2,3,3,1,0};
    data = new char[]{23,164,212,46,244};

  }
  
  @Test
  public void testL2() {
    DNA_SpacedWordFrequencyFilter impl = new DNA_SpacedWordFrequencyFilter(null, 51); //101    
    double[] res = impl.getFrequencies(data);

    double[] resE = new double[]{0000d/18d,2000d/18d,1000d/18d,1000d/18d,2000d/18d
        ,0000d/18d,1000d/18d,2000d/18d,1000d/18d,1000d/18d
        ,1000d/18d,1000d/18d,1000d/18d,2000d/18d,1000d/18d,1000d/18d};
    
    for(int i=0;i<16;i++){
      assertEquals(resE[i], res[i], 0.0001d);
    }
  }

}
