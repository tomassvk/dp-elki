package de.lmu.ifi.dbs.elki.distance.distancefunction.stuba;

import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_DataProvider;

/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * PROTOTYPE ONLY
 * to check importance of lengths in yins approach
 * @author Tomas
 *
 */
public class StringLengthFunction extends AbstractDNAFunction{
  
  protected StringLengthFunction(DNA_DataProvider provider) {
    super(provider);
  }

  @Override
  public double distance(StringVector_externalID o1, StringVector_externalID o2) {
    
    char[] data1 = provider.getData(o1.getFileName(), false, o1.getParams());
    char[] data2 = provider.getData(o2.getFileName(), false, o2.getParams());
    
    return Math.abs(data1.length - data2.length);
  }

  public static class Parameterizer extends AbstractDNAFunction.Parameterizer {
 
    @Override
    protected StringLengthFunction makeInstance() {
      return new StringLengthFunction(provider);
    }

  }

}