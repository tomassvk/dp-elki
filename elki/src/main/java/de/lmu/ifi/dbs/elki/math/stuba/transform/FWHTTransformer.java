package de.lmu.ifi.dbs.elki.math.stuba.transform;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import de.lmu.ifi.dbs.elki.math.stuba.impl.FWHT;

/**
 * 
 * @author Tomas Farkas
 *
 */
public class FWHTTransformer implements SpectralTransformer{

  @Override
  public float[] getSpectrum(boolean direct, boolean clearDC, float[]... arg) {

    int length = 0;
    for(float[] fs : arg) {
      length += fs.length;
    }
    
    float[] result = new float[length];
    
    int pos = 0;
    for(float[] fs : arg) {
      float[] stepRes = FWHT.fht(fs, !direct);
      if(clearDC)
      {      
        stepRes[0] = 0;
      }      
      System.arraycopy(stepRes, 0, result, pos, fs.length);
      pos+=fs.length;
    }   
    
    return result;
    
  }

}
