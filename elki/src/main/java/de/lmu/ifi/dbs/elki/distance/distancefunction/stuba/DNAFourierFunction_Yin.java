package de.lmu.ifi.dbs.elki.distance.distancefunction.stuba;

import de.lmu.ifi.dbs.elki.data.FloatVector;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_DataProvider;
import de.lmu.ifi.dbs.elki.distance.distancefunction.PrimitiveDistanceFunction;
import de.lmu.ifi.dbs.elki.distance.distancefunction.minkowski.EuclideanDistanceFunction;
import de.lmu.ifi.dbs.elki.math.stuba.impl.FFTbase;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.GreaterEqualConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.ObjectParameter;

/**
 * 
 * @author Tomas Farkas, algo by Yin et. al.
 *
 */

// public class DNAFourierFunction_Yin<O> extends
// AbstractPrimitiveDistanceFunction<O> {
public class DNAFourierFunction_Yin extends AbstractDNAFunction {

  protected PrimitiveDistanceFunction<FloatVector> levelDistanceFunction;

  protected int maxSeqLen;

  protected DNAFourierFunction_Yin(DNA_DataProvider provider, PrimitiveDistanceFunction<FloatVector> levelDistanceFunction, int maxSeqLen) {
    super(provider);
    this.levelDistanceFunction = levelDistanceFunction;
    this.maxSeqLen = maxSeqLen;
  }

  @Override
  public double distance(StringVector_externalID o1, StringVector_externalID o2) {

    byte selBase = 0; // lets compute by 'A's

    // A) get data strings
    char[] o1data = provider.getData(o1.getFileName(), false, o1.getParams());
    char[] o2data = provider.getData(o2.getFileName(), false, o2.getParams());

    int o1length = o1data.length;
    int o2length = o2data.length;

    int fftLength = 1 << (int) (Math.log(maxSeqLen - 1) / Math.log(2) + 1);

    // B) compute fft stuff
    //// 1. compute BSI out of dataStrings
    float o1bsi[] = new float[fftLength];
    float normalizedOne = (float) maxSeqLen / (float) o1length;
    for(int i = 0; i < o1length; i++) {
      char curB = o1data[i];
      if(curB == selBase) {
        o1bsi[i] = normalizedOne;
      }
    }

    float o2bsi[] = new float[fftLength];
    normalizedOne = (float) maxSeqLen / (float) o2length;
    for(int i = 0; i < o2length; i++) {
      char curB = o2data[i];
      if(curB == selBase) {
        o2bsi[i] = normalizedOne;
      }
    }
    
    //// 2. compute U = DFT(BSI)
    //// 3. compute PS = U^2
    FFTbase fft = new FFTbase(fftLength, false);
    
    float[] im = new float[fftLength];
    fft.fft(o1bsi, im);
    for(int i=0;i<fftLength;i++){
      //compute PS
      o1bsi[i] = o1bsi[i]*o1bsi[i] + im[i]*im[i];
      // reclear im anyway
      im[i] = 0;
    }
    
    fft.fft(o2bsi, im);
    for(int i=0;i<fftLength;i++){
      //compute PS
      o2bsi[i] = o2bsi[i]*o2bsi[i] + im[i]*im[i];
      // reclear im anyway
      im[i] = 0;
    }
    return levelDistanceFunction.distance(new FloatVector(o1bsi), new FloatVector(o2bsi));

  }

  /**
   * @author Tomas Farkas
   */
  public static class Parameterizer extends AbstractDNAFunction.Parameterizer {

    public static final OptionID SIMILARITY_FUNCTION_ID = new OptionID("dist.distancefunction", "Distance function to resolve number vectors distance.");

    protected PrimitiveDistanceFunction<FloatVector> distanceFunction = null;

    protected static final OptionID LONGEST_SEQ = new OptionID("dist.maxSeqLen", "Length of the lopngest sequence in the database, needed for ELKI architectural reasons.");

    int maxSeqLen = 1;

    @Override
    protected void makeOptions(Parameterization config) {

      final ObjectParameter<PrimitiveDistanceFunction<FloatVector>> param2 = new ObjectParameter<>(SIMILARITY_FUNCTION_ID, PrimitiveDistanceFunction.class, EuclideanDistanceFunction.class);
      if(config.grab(param2)) {
        distanceFunction = param2.instantiateClass(config);
      }

      IntParameter param3 = new IntParameter(LONGEST_SEQ, 1);
      param3.addConstraint(new GreaterEqualConstraint(1));
      if(config.grab(param3)) {
        maxSeqLen = param3.getValue();
      }
    }

    @Override
    protected DNAFourierFunction_Yin makeInstance() {
      return new DNAFourierFunction_Yin(provider, distanceFunction, maxSeqLen);
    }

  }

}
