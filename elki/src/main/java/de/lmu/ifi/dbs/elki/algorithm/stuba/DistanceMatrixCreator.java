package de.lmu.ifi.dbs.elki.algorithm.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.List;

import de.lmu.ifi.dbs.elki.algorithm.AbstractDistanceBasedAlgorithm;
import de.lmu.ifi.dbs.elki.algorithm.Algorithm;
import de.lmu.ifi.dbs.elki.data.type.TypeInformation;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.database.Database;
import de.lmu.ifi.dbs.elki.database.ids.DBIDIter;
import de.lmu.ifi.dbs.elki.database.ids.DBIDUtil;
import de.lmu.ifi.dbs.elki.database.ids.DBIDs;
import de.lmu.ifi.dbs.elki.database.query.distance.DistanceQuery;
import de.lmu.ifi.dbs.elki.database.relation.Relation;
import de.lmu.ifi.dbs.elki.distance.distancefunction.DistanceFunction;
import de.lmu.ifi.dbs.elki.logging.Logging;
import de.lmu.ifi.dbs.elki.math.linearalgebra.Matrix;
import de.lmu.ifi.dbs.elki.result.stuba.MatrixResult;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.ObjectParameter;
import de.lmu.ifi.dbs.elki.visualization.visualizers.stuba.LabelProvider;
import de.lmu.ifi.dbs.elki.visualization.visualizers.stuba.RelationLabelProvider;

/**
 * 
 * @author Tomas
 *
 */
public class DistanceMatrixCreator<O> extends AbstractDistanceBasedAlgorithm<O, MatrixResult> implements Algorithm {

  private static final Logging LOG = Logging.getLogger(DistanceMatrixCreator.class);

  LabelProvider labeller;

  protected DistanceMatrixCreator(DistanceFunction<? super O> distanceFunction, LabelProvider labeller) {
    super(distanceFunction);
    this.labeller = labeller;
  }

  public MatrixResult run(Database database, Relation<O> relation) {
    DBIDs ids = relation.getDBIDs();
    labeller.setDatabase(database);

    int dimension = ids.size();
    double[][] distances = new double[dimension][dimension];

    DistanceQuery<O> distQ = database.getDistanceQuery(relation, getDistanceFunction());

    //get matrix itself
    if(getDistanceFunction().isSymmetric()) {
      int i = 0;
      for(DBIDIter iter1 = ids.iter(); iter1.valid(); iter1.advance()) {
        int j = 0;
        for(DBIDIter iter2 = ids.iter(); iter2.valid(); iter2.advance()) {
          if(i < j) {
            continue;
          }
          distances[j][i] = distances[i][j] = distQ.distance(iter1, iter2);
          j++;
        }
        i++;
      }
    }
    else {
      int i = 0;
      for(DBIDIter iter1 = ids.iter(); iter1.valid(); iter1.advance()) {
        int j = 0;
        for(DBIDIter iter2 = ids.iter(); iter2.valid(); iter2.advance()) {
          distances[i][j] = distQ.distance(iter1, iter2);
          j++;
        }
        i++;
      }
    }

    //get labels
    List<String> labels = new ArrayList<>();
    for(DBIDIter iter = ids.iter(); iter.valid(); iter.advance()) {
      labels.add(labeller.getLabel(DBIDUtil.deref(iter)));
    }
    
    //normalize matrix
    double avg = 0, stdev = 0;
    for(int x = 0; x < distances.length; x++) {
      for(int y = 0; y < distances.length; y++) {
        if(x == y) {
          continue;// avoid selecting diagonal '0's
        }
        avg+=distances[x][y];          
      }
    }
    avg = avg / (distances.length*distances.length-distances.length);
    
    for(int x = 0; x < distances.length; x++) {
      for(int y = 0; y < distances.length; y++) {
        if(x == y) {
          continue;// avoid selecting diagonal '0's
        }
        stdev+=(distances[x][y] - avg)*(distances[x][y] - avg);          
      }
    }
    stdev = stdev / (distances.length*distances.length-distances.length);
    stdev = Math.sqrt(stdev);
    
    for(int x = 0; x < distances.length; x++) {
      for(int y = 0; y < distances.length; y++) {
        distances[x][y] = x==y ? 0 : 10 + (distances[x][y] - avg) / stdev;
      }
    }

    return new MatrixResult(new Matrix(distances), labels.toArray(new String[0]), labels.toArray(new String[0]));
  }

  @Override
  public TypeInformation[] getInputTypeRestriction() {
    return TypeUtil.array(getDistanceFunction().getInputTypeRestriction());
  }

  @Override
  protected Logging getLogger() {
    return LOG;
  }

  public static class Parameterizer<O> extends AbstractDistanceBasedAlgorithm.Parameterizer<O> {

    public static final OptionID LABEL_PROVIDER_ID = new OptionID("algorithm.labeller", "The class to provide labels for DB objects.");

    LabelProvider labeller;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);
      final ObjectParameter<LabelProvider> param = new ObjectParameter<>(LABEL_PROVIDER_ID, LabelProvider.class, RelationLabelProvider.class);
      if(config.grab(param)) {
        labeller = (LabelProvider) param.instantiateClass(config);
      }
    }

    @Override
    protected DistanceMatrixCreator<O> makeInstance() {
      return new DistanceMatrixCreator<>(distanceFunction, labeller);
    }
  }

}
