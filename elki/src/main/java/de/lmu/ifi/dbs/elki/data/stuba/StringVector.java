package de.lmu.ifi.dbs.elki.data.stuba;

import java.util.List;

import de.lmu.ifi.dbs.elki.data.FeatureVector;
import de.lmu.ifi.dbs.elki.data.type.VectorFieldTypeInformation;

/**
 * Type to describe String Vector
 * @see VectorFieldTypeInformation
 * @author Tomas Farkas 
 */
public interface StringVector extends FeatureVector<String>{
  
  String[] toArray();  
  
  interface Factory<V extends StringVector> extends FeatureVector.Factory<V, String> {
    
    V newStringVector(String[] values);
    
    V newStringVector(List<String> values);

    V newStringVector(StringVector values);   
  }
  
  
}
