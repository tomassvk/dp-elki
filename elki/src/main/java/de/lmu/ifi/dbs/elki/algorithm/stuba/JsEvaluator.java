package de.lmu.ifi.dbs.elki.algorithm.stuba;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.lmu.ifi.dbs.elki.algorithm.AbstractAlgorithm;
import de.lmu.ifi.dbs.elki.algorithm.Algorithm;
import de.lmu.ifi.dbs.elki.data.type.TypeInformation;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.database.Database;
import de.lmu.ifi.dbs.elki.database.ids.DBIDIter;
import de.lmu.ifi.dbs.elki.database.relation.Relation;
import de.lmu.ifi.dbs.elki.logging.Logging;
import de.lmu.ifi.dbs.elki.result.Result;
import de.lmu.ifi.dbs.elki.result.stuba.MatrixResult;
import de.lmu.ifi.dbs.elki.result.stuba.StringResult;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.FileParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.FileParameter.FileType;

/**
 * 
 * Interprets specified script in Nashorn JS engine
 * data bindings for input:
 * var jsonInput = JSON.parse(input);
 * 
 * data bindings for output:
 * var result = ...
 * 
 * @author Tomas Farkas
 *
 */
public class JsEvaluator extends AbstractAlgorithm<MatrixResult> implements Algorithm {

  protected File jsFile;

  private static final Logging LOG = Logging.getLogger(JsEvaluator.class);

  public JsEvaluator(File jsFile) {
    super();
    this.jsFile = jsFile;
  }

  @SuppressWarnings("rawtypes")
  public Result run(Database database, Relation relation) throws JsonProcessingException, ScriptException, FileNotFoundException {

    final ScriptEngineManager eFactory = new ScriptEngineManager();
    final ScriptEngine engine = eFactory.getEngineByName("JavaScript");
    final Bindings eBinds = engine.getBindings(ScriptContext.ENGINE_SCOPE);
    ObjectMapper jsonMapper = new ObjectMapper();
    jsonMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
    
    Map<String, List<Object>> input = new HashMap<>();
    for(Relation r : database.getRelations()) {
      List<Object> data = new ArrayList<>();
      for(DBIDIter iter = r.iterDBIDs();iter.valid();iter.advance()){
        data.add(r.get(iter));
      }
      input.put(r.getShortName(), data);
    }
    eBinds.put("input", jsonMapper.writeValueAsString(input));
    
    engine.eval(new FileReader(jsFile));
  
    String result = "";
    try{
      result = (String) eBinds.get("result");
      if(result==null){
        LOG.warning("Result not present after evaluating js script");
        result = "";
      }
    }catch(Exception e){
      LOG.warning("Result not present after evaluating js script");
    }
        
    return new StringResult(result);

  }

  @Override
  public TypeInformation[] getInputTypeRestriction() {
    return TypeUtil.array(TypeUtil.ANY);
  }

  @Override
  protected Logging getLogger() {
    return LOG;
  }

  public static class Parameterizer<O> extends AbstractParameterizer {

    protected static final OptionID JSFILE = new OptionID("algo.jsFile", "Script to evaluate");

    protected File jsFile;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      FileParameter param = new FileParameter(JSFILE, FileType.INPUT_FILE);
      if(config.grab(param)) {
        jsFile = param.getValue();
      }
    }

    @Override
    protected JsEvaluator makeInstance() {
      return new JsEvaluator(jsFile);
    }
  }

}
