package de.lmu.ifi.dbs.elki.distance.distancefunction.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.data.type.SimpleTypeInformation;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_DataProvider;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.RandomDNADataProvider;
import de.lmu.ifi.dbs.elki.distance.distancefunction.AbstractPrimitiveDistanceFunction;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.ObjectParameter;

/**
 * Common methods for all distance functions working with DNA data described by
 * external String vector
 * 
 * @author Tomas Farkas
 *
 */
public abstract class AbstractDNAFunction extends AbstractPrimitiveDistanceFunction<StringVector_externalID> {

  protected DNA_DataProvider provider;

  protected AbstractDNAFunction(DNA_DataProvider provider) {
    this.provider = provider;
  }

  @Override
  public SimpleTypeInformation<StringVector_externalID> getInputTypeRestriction() {
    return (SimpleTypeInformation<StringVector_externalID>) TypeUtil.STRING_VECTOR_EXTERNAL;
  }

  public static abstract class Parameterizer extends AbstractParameterizer {

    protected static final OptionID DATA_PROVIDER_ID = new OptionID("dist.dataProvider", "Data provider providing DNA 2-bit encoded string array");
    protected DNA_DataProvider provider = null;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);
      final ObjectParameter<DNA_DataProvider> param = new ObjectParameter<>(DATA_PROVIDER_ID, DNA_DataProvider.class, RandomDNADataProvider.class);
      if(config.grab(param)) {
        provider = (DNA_DataProvider) param.instantiateClass(config);
      }
    }   

  }
}
