package de.lmu.ifi.dbs.elki.datasource.filter.stuba;

import java.io.File;
import java.util.List;

import de.lmu.ifi.dbs.elki.data.stuba.StringVector;
import de.lmu.ifi.dbs.elki.data.type.SimpleTypeInformation;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.bundle.MultipleObjectsBundle;
import de.lmu.ifi.dbs.elki.datasource.filter.ObjectFilter;
import de.lmu.ifi.dbs.elki.logging.Logging;
import de.lmu.ifi.dbs.elki.result.stuba.MatrixReader;

/**
 * Ugly prototype, not for release
 * 
 * @author Tomas Farkas
 *
 */
public class TreeAbsoluteComparator implements ObjectFilter {

  protected static final Logging LOG = Logging.getLogger(TreeAbsoluteComparator.class);

  protected MultipleObjectsBundle inputBundle;

  MatrixReader mr = new MatrixReader();

  protected List<StringVector> inputVectors = null;

  @SuppressWarnings("unchecked")
  @Override
  public MultipleObjectsBundle filter(MultipleObjectsBundle objects) {

    inputBundle = objects;

    int validEntryColumnCount = 0;

    for(int r = 0; r < objects.metaLength(); r++) {
      final SimpleTypeInformation<?> type = objects.meta(r);
      final List<?> column = objects.getColumn(r);

      if(TypeUtil.STRING_VECTOR.isAssignableFromType(type)) {
        validEntryColumnCount++;
        inputVectors = ((List<StringVector>) column);
      }
    }
    if(validEntryColumnCount != 1) {
      return null;
    }

    for(StringVector stringVector : inputVectors) {
      String diff = getDiff(stringVector);
      LOG.info(stringVector.getValue(0));
      LOG.info(diff);
      LOG.info(" ");
      System.out.print(stringVector.getValue(0));
      System.out.println(" "+diff);
    }
    return new MultipleObjectsBundle();
  }

  private String getDiff(StringVector in) {

    double[][] ent1 = mr.readMatrix(new File(in.getValue(1)), Integer.parseInt(in.getValue(2)), Integer.parseInt(in.getValue(3))).getMatrix().getArrayRef();
    double[][] ent2 = mr.readMatrix(new File(in.getValue(4)), Integer.parseInt(in.getValue(5)), Integer.parseInt(in.getValue(6))).getMatrix().getArrayRef();

    double res = 0;
    int nmatch = 0;
    
    for(int i = 0; i < ent1.length; i++) {
      for(int j = 0; j < ent1[0].length; j++) {
        res += Math.abs(ent1[i][j] - ent2[i][j]);
        if(ent1[i][j] == ent2[i][j]){
          nmatch++;
        }
      }
    }
    
    return res+" "+nmatch;

  }

}
