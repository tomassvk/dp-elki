package de.lmu.ifi.dbs.elki.algorithm.clustering.hierarchical.stuba;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.lmu.ifi.dbs.elki.algorithm.AbstractDistanceBasedAlgorithm;
import de.lmu.ifi.dbs.elki.algorithm.clustering.hierarchical.HierarchicalClusteringAlgorithm;
import de.lmu.ifi.dbs.elki.algorithm.clustering.hierarchical.PointerHierarchyRepresentationResult;
import de.lmu.ifi.dbs.elki.data.type.TypeInformation;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.database.Database;
import de.lmu.ifi.dbs.elki.database.ids.ArrayDBIDs;
import de.lmu.ifi.dbs.elki.database.ids.DBID;
import de.lmu.ifi.dbs.elki.database.ids.DBIDUtil;
import de.lmu.ifi.dbs.elki.database.query.distance.DistanceQuery;
import de.lmu.ifi.dbs.elki.database.relation.Relation;
import de.lmu.ifi.dbs.elki.distance.distancefunction.DistanceFunction;
import de.lmu.ifi.dbs.elki.logging.Logging;

/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 * @author Tomas Farkas adapted from
 * 
 *         Copyright (c) 2014. Real Time Genomics Limited.
 *
 *  and modified for integration with ELKI
 */
public class NeighborJoining<O> extends AbstractDistanceBasedAlgorithm<O, PointerHierarchyRepresentationResult> implements HierarchicalClusteringAlgorithm {

  private static final Logging LOG = Logging.getLogger(NeighborJoining.class);

  ///////////////////////////////////////////////
  //////////// ELKI ADAPTER//////////////////////

  public NeighborJoining(DistanceFunction<? super O> distanceFunction) {
    super(distanceFunction);
  }

  @SuppressWarnings("deprecation")
  public PointerHierarchyRepresentationResult run(Database database, Relation<O> relation) {
    DistanceQuery<O> dq = database.getDistanceQuery(relation, getDistanceFunction());
    ArrayDBIDs ids = DBIDUtil.ensureArray(relation.getDBIDs());

    int size = ids.size();
    DBID[] dbidA = new DBID[size];
    double[][] distMx = new double[size][size];

    for(int i = 0; i < size; i++) {
      dbidA[i] = ids.get(i);
    }

    for(int i = 0; i < size; i++) {
      for(int j = 0; j < size; j++) {
        if(i != j) {
          distMx[i][j] = dq.distance(dbidA[i], dbidA[j]);
        }
      }
    }

    
    BinaryTreeResult result = neighborJoin(Arrays.asList(dbidA), distMx);
    System.out.println(result.toNewick());
    return result.getPHRR(ids);

  }

  @Override
  public TypeInformation[] getInputTypeRestriction() {
    return TypeUtil.array(getDistanceFunction().getInputTypeRestriction());
  }

  @Override
  protected Logging getLogger() {
    return LOG;
  }

  //////////////////////////////////////////////////////
  //////////// MODIFIED IMPLEMENTATION//////////////////

  /**
   * Perform the neighbor-joining algorithm. Given a list of node names and a
   * matrix of distances between sequences, infer a binary tree using the
   * neighbor-joining algorithm.
   *
   * @param nodeNames the list of node names for translating back from the
   *        matrix.
   * @param matrix count of number of hashes in common between pairs of
   *        sequences.
   * @return the resulting tree
   */
  public BinaryTreeResult neighborJoin(final List<DBID> nodeIDs, final double[][] matrix) {
    final ArrayList<ArrayList<Double>> d = makeArray(matrix);
    return neighborJoin(nodeIDs, d);
  }

  /* Retrieve the distance between two taxa. */
  private static double distance(final int i, final int j, final ArrayList<ArrayList<Double>> d) {
    return i == j ? 0 : (i > j ? d.get(i).get(j) : d.get(j).get(i));
  }

  /* Remove a given item from the distance matrix. */
  private static void remove(final int f, final ArrayList<BinaryTreeResult> s, final ArrayList<ArrayList<Double>> d) {
    for(int k = f + 1; k < d.size(); k++) {
      d.get(k).remove(f);
    }
    s.remove(f);
    d.remove(f);
  }

  /**
   * Produce an array which is a lower triangular matrix with normalized and
   * inverted similarity scores (the matrix contains counts of common hashes for
   * pairs of sequences).
   * 
   * @param matrix with the original counts.
   * @return the lower triangular array.
   */
  static ArrayList<ArrayList<Double>> makeArray(final double[][] matrix) {
    final ArrayList<ArrayList<Double>> d;
    final int length = matrix.length;
    // compute normalization factors
    final double[] norm = new double[length];
    for(int i = 0; i < length; i++) {
      // be careful to deal with 0's on diagonal
      final double n = matrix[i][i];
      assert n >= 0;
      norm[i] = Math.sqrt(n == 0 ? 1 : n);
    }
    d = new ArrayList<>();
    for(int i = 0; i < length; i++) {
      final ArrayList<Double> row = new ArrayList<>();
      for(int j = 0; j < i; j++) {
        final double v = matrix[i][j] / (norm[i] * norm[j]);
        assert v >= 0.0 && !Double.isInfinite(v) && !Double.isNaN(v) : v;
        //final double w = 1.0 / (1.0 + v);
        //assert w >= 0.0 && !Double.isInfinite(w) && !Double.isNaN(w) : w;
        row.add(v);
      }
      d.add(row);
    }   
    return d;
  }

  /**
   * Perform the neighbor-joining algorithm. Given a list of node names and a
   * lower triangular matrix of distances between names, infer a binary tree
   * using the neighbor-joining algorithm.
   *
   * @param nodeNames the node names
   * @param d distance matrices
   * @return the resulting tree
   */
  BinaryTreeResult neighborJoin(final List<DBID> nodeIDs, final ArrayList<ArrayList<Double>> d) {
    final ArrayList<BinaryTreeResult> s = new ArrayList<>();
    
    for(final DBID id : nodeIDs) {
      s.add(new BinaryTreeResult(null, null, 0, 0, id));
    }
    while(s.size() > 1) {
      assert d.size() == s.size() : "distance size=" + d.size() + " cf. names size=" + s.size();
      // Precompute column sums
      final double[] colSum = new double[d.size()];
      for(int j = 0; j < d.size(); j++) {
        double sum = 0;
        for(int k = 0; k < d.size(); k++) {
          sum += distance(j, k, d);
        }
        colSum[j] = sum;
      }

      // Compute minimal entry in (implicit) Q matrix
      final int r = d.size() - 2;
      double best = Double.POSITIVE_INFINITY;
      int c = 0;
      int f = -1;
      int g = -1;
      for(int k = 1; k < d.size(); k++) {
        for(int j = 0; j < k; j++) {
          final double q = r * distance(k, j, d) - colSum[k] - colSum[j];
          if(Double.doubleToRawLongBits(q) == Double.doubleToRawLongBits(best) /*&& nextInt(++c) == 0*/) {
            // Break ties fairly
            f = k;
            g = j;
          }
          else if(q < best) {
            c = 1;
            f = k;
            g = j;
            best = q;
          }
        }
      }
      assert f >= 0 && g >= 0 : "f=" + f + " g=" + g;

      // Compute distance of merged node to new node
      final double dfu, dgu, dfg = 0.5 * distance(f, g, d);
      if(d.size() > 2) {
        dfu = dfg + 0.5 * (colSum[f] - colSum[g]) / (d.size() - 2);
        dgu = dfg + 0.5 * (colSum[g] - colSum[f]) / (d.size() - 2);
      }
      else {
        assert Math.abs(colSum[f] - colSum[g]) < 0.0000000001;
        dfu = dfg;
        dgu = dfg;
      }

      // Compute distance of merged node to all other nodes
      final ArrayList<Double> newRow = new ArrayList<>();
      for(int i = 0; i < d.size(); i++) {
        if(i != f && i != g) {
          newRow.add(0.5 * (distance(f, i, d) - dfu + distance(g, i, d) - dgu));
        }
      }
      final BinaryTreeResult newNode = new BinaryTreeResult(s.get(f), s.get(g), dfu, dgu, null);

      // Remove merged nodes starting with the larger one
      if(f > g) {
        remove(f, s, d);
        remove(g, s, d);
      }
      else {
        remove(g, s, d);
        remove(f, s, d);
      }

      // Add combined node at end of name list and matrix
      assert newRow.size() == d.size();
      d.add(newRow);
      s.add(newNode);
    }
    return s.get(0);
  }

  private int nextInt(int exclMaxBound) {
    return (int) (Math.random() * exclMaxBound);
  }

  public static class Parameterizer<O> extends AbstractDistanceBasedAlgorithm.Parameterizer<O> {
    @Override
    protected NeighborJoining<O> makeInstance() {
      return new NeighborJoining<>(distanceFunction);
    }
  }

}
