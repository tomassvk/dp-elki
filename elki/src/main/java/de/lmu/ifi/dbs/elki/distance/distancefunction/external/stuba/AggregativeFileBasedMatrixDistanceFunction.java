package de.lmu.ifi.dbs.elki.distance.distancefunction.external.stuba;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.SubEqualString;
import de.lmu.ifi.dbs.elki.logging.Logging;
import de.lmu.ifi.dbs.elki.result.stuba.MatrixReader;
import de.lmu.ifi.dbs.elki.result.stuba.MatrixResult;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.DoubleListParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.FileListParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.ObjectParameter;
import de.lmu.ifi.dbs.elki.visualization.visualizers.stuba.LabelProvider;
import de.lmu.ifi.dbs.elki.visualization.visualizers.stuba.RelationLabelProvider;

/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2017
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 * @author Tomas Farkas
 *
 */

public class AggregativeFileBasedMatrixDistanceFunction extends FileBasedMatrixDistanceFunction {

  private static final Logging LOG = Logging.getLogger(AggregativeFileBasedMatrixDistanceFunction.class);

  
  List<File> matrixFiles;
  double[] mxWeights;

  public AggregativeFileBasedMatrixDistanceFunction(List<File> matrixFiles, double[] mxWeights) {
    this.matrixFiles = matrixFiles;
    this.mxWeights = mxWeights;
    if(matrixFiles.size()!=mxWeights.length){
      LOG.warning("Expected "+matrixFiles.size()+" weigths, got "+mxWeights.length);
      this.mxWeights = new double[matrixFiles.size()];
      Arrays.fill(this.mxWeights, 1.0);
    }
    buildTable();
  }

  private void buildTable() {
    
    for(int f=0;f<matrixFiles.size();f++) {
      File file = matrixFiles.get(f);
      MatrixResult distM = (new MatrixReader()).readMatrix(file);

      String[] labels = distM.getLabelsC();
      double[][] matrix = distM.getMatrix().getArrayRef();

      for(int i = 0; i < labels.length; i++) {

        Map<SubEqualString, Double> row = distances.get(new SubEqualString(labels[i]));
        if(row == null) {
          row = new HashMap<>();
          distances.put(new SubEqualString(labels[i]), row);
        }

        for(int j = 0; j < labels.length; j++) {

          Double entry = row.get(new SubEqualString(labels[j]));
          if(entry == null) {
            entry = 0.0;
          }
          entry += matrix[i][j] * mxWeights[f];
          
          row.put(new SubEqualString(labels[j]), entry);
        }
      }
    }
  }

  public static class Parameterizer extends AbstractParameterizer {

    public static final OptionID MATRIX_ID = new OptionID("distance.matrix", "The names of the files containing the distance matrix.");

    protected List<File> matrixfiles = null;
    
    public static final OptionID MX_WEIGHTS = new OptionID("distance.mxWeights", "Weights of matrices");

    protected double[] mxWeights = null;
    
    public static final OptionID LABEL_PROVIDER_ID = new OptionID("distance.labeller", "The class to provide labels for DB objects.");

    LabelProvider labeller;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);
      final FileListParameter MATRIX_PARAM = new FileListParameter(MATRIX_ID, FileListParameter.FilesType.INPUT_FILES);
      if(config.grab(MATRIX_PARAM)) {
        matrixfiles = MATRIX_PARAM.getValue();
      }
      
      final DoubleListParameter MX_WEIGHT_PARAM = new DoubleListParameter(MX_WEIGHTS);
      if(config.grab(MX_WEIGHT_PARAM)) {
        mxWeights = MX_WEIGHT_PARAM.getValue();
      }

      final ObjectParameter<LabelProvider> param = new ObjectParameter<>(LABEL_PROVIDER_ID, LabelProvider.class, RelationLabelProvider.class);
      if(config.grab(param)) {
        labeller = (LabelProvider) param.instantiateClass(config);
      }
    }

    @Override
    protected AggregativeFileBasedMatrixDistanceFunction makeInstance() {
      return new AggregativeFileBasedMatrixDistanceFunction(matrixfiles, mxWeights);
    }
  }

}
