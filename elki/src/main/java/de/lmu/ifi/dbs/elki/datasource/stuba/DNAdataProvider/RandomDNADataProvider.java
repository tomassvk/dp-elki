
package de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider;

/**
 * DNADataProvider implemetation that creates random string of DNA data Test
 * only - deprecated
 * 
 * @author Tomas Farkas
 *
 */
public class RandomDNADataProvider implements DNA_DataProvider {

  @Override
  public char[] getData(String fname, boolean dense, String ... params) {

    //1st param as length
    String param1 = params[0];
    int DATA_LENGTH = Integer.parseInt(param1);
   
    char[] res = new char[DATA_LENGTH];
    //4 bases per byte (only A, C, G, T, no N/A indicators, no extended alphabet)
    if(dense) {
      for(int i = 0; i < res.length; i++) {
        res[i] = (char) (Math.random() * 256);
      }
      if(DATA_LENGTH < 9) {

        System.out.print("RandomDNADataProvider: ");
        for(char b : res) {
          for(int i = 0; i < 4; i++) {
            if((b & 192) == 0) {
              System.out.print('A');
            }
            else if((b & 192) == 64) {
              System.out.print('C');
            }
            else if((b & 192) == 128) {
              System.out.print('G');
            }
            else {
              System.out.print('T');
            }
            b = (char) (b << 2);
          }
          System.out.print(" ");
        }
        System.out.println("");
      }
    }
    // 1 base per 1 byte (general use)
    else {
      for(int i = 0; i < res.length; i++) {
        res[i] = (char) (Math.random() * 4);
      }
      if(DATA_LENGTH < 33) {
        System.out.print("RandomDNADataProvider: ");
        for(int i = 0; i < DATA_LENGTH; i++) {
          char b = res[i];
          if(b == 0) {
            System.out.print('A');
          }
          else if(b == 1) {
            System.out.print('C');
          }
          else if(b == 2) {
            System.out.print('G');
          }
          else {
            System.out.print('T');
          }
          if(((i + 1) % 4) == 0) {
            System.out.print(" ");
          }
        }
        System.out.println("");
      }
    }

    return res;
  }

}
